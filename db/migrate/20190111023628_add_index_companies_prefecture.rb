class AddIndexCompaniesPrefecture < ActiveRecord::Migration[5.1]
  def change
  	add_index :companies, :prefecture
  end
end
