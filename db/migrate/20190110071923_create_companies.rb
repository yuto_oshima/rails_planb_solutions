class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :description
      t.string :category
      t.string :prefecture
      t.string :employee
      t.string :detail_url
      t.string :img_url

      t.timestamps
    end
  end
end
