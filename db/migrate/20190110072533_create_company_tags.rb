class CreateCompanyTags < ActiveRecord::Migration[5.1]
  def change
    create_table :company_tags do |t|
      t.references :company, foreign_key: true
      t.string :tag

      t.timestamps
    end
  end
end
