Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "event#mainabi"
  get "event/mainabi_links"
  post "users/favorites"
  post "users/recommend"
  post "user_favorites/index"
  post "user_favorites/create"
  post "user_favorites/destroy"
  post "companies/index"
  post "companies/search"
end
