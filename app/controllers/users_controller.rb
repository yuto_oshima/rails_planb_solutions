class UsersController < ApplicationController

	def favorites
		user = find_or_create_user
		user_favorites = user.user_favorites
		companies = Company.where(id: user_favorites.pluck(:company_id))
		result = {}
		result["companies"] = companies
		render json: result
	end

	def recommend 
		result = {}
		exceptions = ["2018", "6", "!!", ".", "8", "72", "NHK", "!!】", "(", ")", "仕事", "企業", "サービス", "日", "一式", 
									"。", "、", ",",
									"北海道", "青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県", 
									"茨城県", "栃木県", "群馬県", "埼玉県", "千葉県", "東京都", "神奈川県",
									"新潟県", "富山県", "石川県", "福井県", "山梨県", "長野県", "岐阜県", 
									"静岡県", "愛知県", "三重県", "滋賀県", "京都府", "大阪府", "兵庫県", 
                	"奈良県", "和歌山県", "鳥取県", "島根県", "岡山県", "広島県", "山口県",
                	"徳島県", "香川県", "愛媛県", "高知県", "福岡県", "佐賀県", "長崎県", 
                	"熊本県", "大分県", "宮崎県", "鹿児島県", "沖縄県", "宮城"]
		user = find_or_create_user
		user_favorites = user.user_favorites
		favorite_companies = Company.where(id: user_favorites.pluck(:company_id))
		favorite_company_tags = CompanyTag.where(company_id: favorite_companies.pluck(:id)).where.not(tag: exceptions).having("SUM(count) > 1").limit(5).order("sum_count desc").group(:tag).sum(:count)
		recommend_company_ids = CompanyTag.where(tag: favorite_company_tags.keys).where.not(company_id: favorite_companies.pluck(:id)).where("count > 2").pluck(:company_id)
		recommend = Company.where(id: recommend_company_ids)
		result["companies"] = recommend
		render json: result
	end

end
