class UserFavoritesController < ApplicationController

	def index
		user = find_or_create_user
		user_favorites = user.user_favorites
		result = {}
		result["user_favorites"] = user_favorites
		render json: result
	end

	def create
		user = find_or_create_user
		user_favorite = user.user_favorites.build(company_id: params[:company_id])
		user_favorite.save
		render json: user_favorite
	end

	def destroy
		user = find_or_create_user
		user.user_favorites.find_by(company_id: params[:company_id]).destroy
		render json: {"response": "success"}
	end

end
