require 'open-uri'
require 'nokogiri'
require 'mechanize'
require 'selenium-webdriver'
require 'natto'
require 'mecab'
class EventController < ApplicationController

	def open_url(url)
		charset = nil
		html = open(url) do |f|
			charset = f.charset
			f.read
		end
		Nokogiri::HTML.parse(html, nil, charset)
	end

	def mainabi
		events = []
		companies = []
		existing_urls = Company.all.pluck(:detail_url)
		driver = Selenium::WebDriver.for(:firefox)
		driver.navigate.to "https://job.mynavi.jp/20/pc/corpinfo/displayCorpSearch/index"
		driver.find_element(id: "areaCategory0").click
			driver.find_element(id: "ifRegional510").click
#		driver.find_element(xpath: "//*[@id='areaGroup0']/div/p/span[1]/a").click

# 		driver.find_element(id: "areaCategory1").click
# 		driver.find_element(xpath: "//*[@id='areaGroup1']/div/p/span[1]/a").click
#
# 		driver.find_element(id: "areaCategory2").click
# 		driver.find_element(xpath: "//*[@id='areaGroup2']/div/p/span[1]/a").click
#
# 		driver.find_element(id: "areaCategory3").click
# 		driver.find_element(xpath: "//*[@id='areaGroup3']/div/p/span[1]/a").click
#
# 		driver.find_element(id: "areaCategory4").click
# 		driver.find_element(xpath: "//*[@id='areaGroup4']/div/p/span[1]/a").click
#
# 		driver.find_element(id: "areaCategory5").click
# 		driver.find_element(xpath: "//*[@id='areaGroup5']/div/p/span[1]/a").click
#
# 		driver.find_element(id: "areaCategory6").click
# 		driver.find_element(xpath: "//*[@id='areaGroup6']/div/p/span[1]/a").click

		driver.find_element(:id => "doSearchTypeIndustryArea2").click
		wait = Selenium::WebDriver::Wait.new(timeout: 3)

		
		sleep(3)
		flag = true
		while flag
			sleep(3)

			corp_labeles = driver.find_elements(xpath: "//div[@class='boxSearchresultEach corp label']")
			corp_labeles.each do |cp|
				company = Company.new
				company.detail_url = cp.find_element(xpath: ".//h3/a").attribute("href")
				company.name = cp.find_element(xpath: ".//h3/a").text
				if cp.find_elements(xpath: ".//div[2]//div[@class='txt']").count == 0
					company.description = cp.find_element(xpath: ".//h4").text unless cp.find_elements(xpath: ".//h4").count == 0
				else
					company.description = cp.find_element(xpath: ".//div[2]//div[@class='txt']/h4").text unless cp.find_elements(xpath: ".//h4").count == 0
					company.description += cp.find_element(xpath: ".//div[2]//div[@class='txt']/p").text unless cp.find_elements(xpath: ".//div[2]//div[@class='txt']/p").count == 0
					company.img_url = cp.find_element(xpath: ".//img").attribute("src") unless cp.find_elements(xpath: ".//img").count == 0
				end
				company.category = cp.find_element(xpath: ".//div[@class='linkCtgWrap']/div[1]//dd//tt").text
				company.category += cp.find_element(xpath: ".//div[@class='linkCtgWrap']/div[1]//dd/span[@class='other_job']").text unless cp.find_elements(xpath: ".//div[@class='linkCtgWrap']/div[1]//dd/span[@class='other_job']").count == 0
				company.prefecture = cp.find_element(xpath: ".//div[@class='linkCtgWrap']/div[2]//dd[1]/span").text
				company.employee = cp.find_element(xpath: ".//div[@class='linkCtgWrap']/div[2]//dd[2]/span").text
				companies << company unless existing_urls.include?(company.detail_url)
			end
			if driver.find_elements(xpath: "//*[@id='contentsleft']/div[1]/div[2]/ul/li[@class='right noLink']").count == 0
				driver.find_element(id: "upperNextPage").click
				wait = Selenium::WebDriver::Wait.new(timeout: 3)
			else 
				flag = false
			end
		end 
		Company.import companies
		render json: JSON.pretty_generate(companies.as_json)
	end
	
	def mainabi_links
		nm = MeCab::Tagger.new("-Ochasen")
		existing_tags = CompanyTag.all.pluck(:tag)
		companies = Company.all
		company_tags = [] 
		companies.each do |company|
			company_tag_hash = {}
			unless company.description.blank?
				node = nm.parseToNode(company.description)
				while node
					if /^名詞/ =~ node.feature.split(/,/)[0] then  
						if company_tag_hash.key?(node.surface)
							company_tag_hash[node.surface] = company_tag_hash[node.surface] + 1
						else 
							company_tag_hash[node.surface] = 1
  		    	end  
  		  	end
  		  	node = node.next
  		  end 
  		  company_tag_hash.each do |key, value|
  		  	company_tag = company.company_tags.build
  		  	company_tag.tag = key
  		  	company_tag.count = value
  		  	company_tags << company_tag unless existing_tags.include?(company_tag.tag)
  	 		end
  	 	end
		end

    render json: JSON.pretty_generate(company_tags.as_json)
	end


end
