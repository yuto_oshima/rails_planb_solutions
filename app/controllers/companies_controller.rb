class CompaniesController < ApplicationController

	# 登録されている全ての会社情報を取得する
	def index
		cp = Company.all
		result = {}
		result["companies"] = cp 
		render json: JSON.pretty_generate(result.as_json)
	end

	# パラメータから送られた情報を元に検索をかける
	def search
		result = {}
		case params[:attribute]
		when "description" then
			result["companies"] = Company.where("description like ? or name like ?", "%#{params[:keyword]}%", "%#{params[:keyword]}%")
		when "prefecture" then
			result["companies"] = Company.where("prefecture like ?", "%#{params[:keyword]}%")
		when "category" then
			result["companies"] = Company.where("category like ?", "%#{params[:keyword]}%")
		end
		
		render json: JSON.pretty_generate(result.as_json)
	end

end
