class Company < ApplicationRecord
	has_many :company_tags
	has_many :user_favorites
end
