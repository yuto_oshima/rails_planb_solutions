namespace :mecab do
	desc "scrapeした概要を形態素解析する"
	task :analyze => :environment do
		nm = MeCab::Tagger.new("-Ochasen")
		existing_tags = CompanyTag.all.pluck(:tag)
		companies = Company.all
		company_tags = [] 
		companies.each do |company|
			company_tag_hash = {}
			unless company.description.blank?
				node = nm.parseToNode(company.description)
				while node
					if /^名詞/ =~ node.feature.split(/,/)[0] then  
						if company_tag_hash.key?(node.surface)
							company_tag_hash[node.surface] = company_tag_hash[node.surface] + 1
						else 
							company_tag_hash[node.surface] = 1
  		    	end  
  		  	end
  		  	node = node.next
  		  end 
  		  company_tag_hash.each do |key, value|
  		  	company_tag = company.company_tags.build
  		  	company_tag.tag = key
  		  	company_tag.count = value
  		  	company_tags << company_tag unless existing_tags.include?(company_tag.tag)
  	 		end
  	 	end
		end

   CompanyTag.import company_tags
	end
end
